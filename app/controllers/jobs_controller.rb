class JobsController < ApplicationController

	before_action :find_jobs, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_company!, except: [:index, :show]

	def index
		@jobs = Job.all.order("created_at DESC")
		@category = Category.all
	end

	def show
	end

	def new
		@job = current_company.jobs.build
		#@job = current_user.job.build
	end

	def create
		@job = current_company.jobs.build(jobs_params)

		if @job.save
			redirect_to @job
		else 
			render "New"
		end
	end

	def edit
		
	end

	def update
		if @job.update(jobs_params)
			redirect_to @job
		else 
			render "New"
		end
	end

	def destroy
		@job.destroy
		redirect_to root_path
	end

	private

	def jobs_params
		params.require(:job).permit(:title, :from_company, :website, :hq, :description, :banner, :url, :category_id, :company_id)
	end

	def find_jobs
		@job = Job.find(params[:id])
	end
end
