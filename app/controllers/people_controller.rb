class PeopleController < ApplicationController
  def index
    if params[:tag]
      @people = Person.tagged_with(params[:tag])
    else
      @people = Person.all
    end
  end

  def show
  end

  def crate
  end

  def new
  end

  def edit
  end

  def update
  end

  def user_params
    params.require(:person).permit(:tag_list)
  end
end
