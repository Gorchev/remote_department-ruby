$(document).ready(function(){

	$(".profile_wrapper .profile:nth-child(1)").addClass("current");

	$(".profile_wrapper .profile").each(function(i){
		$(this).attr("id", (i+1).toString());
	});


	var windowHeight = $(window).height(); //Taking the height of the window

	sliderInt = 1;
	sliderNext = 2;

	count = $(".profile").size();

	function leftArrowPressed() {
	   newSlide = sliderInt - 1;
	   showSlide(newSlide);
	}

	function rightArrowPressed() {
	   newSlide = sliderInt + 1;
	   showSlide(newSlide);
	}

	function showSlide(id) {
		if(id > count){
			id = 1;
		} else if(id < 1) {
			id = count;
		}

		$(".profile").removeClass("current");
		$(".profile#"+id).addClass("current");

		sliderInt = id;
		sliderNext = id + 1;
	}

	document.onkeydown = function(evt) {
	    evt = evt || window.event;
	    switch (evt.keyCode) {
	        case 37:
	            leftArrowPressed();
	            break;
	        case 39:
	            rightArrowPressed();
	            break;
	    }
	};

	//Animate when go through the profiles
	$(".profile").click(function(){
		$(".profile").removeClass("current");
		$(this).addClass("current");
	});
	

});