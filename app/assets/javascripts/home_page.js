$(document).ready(function(){
	var windowHeight = $(window).height(); //Taking the height of the window

	//Make the home three section matching the height 
	//of the window minus 72px of the header
	$(".welcome_part").height(windowHeight - 72)

	//On hover over each section resize the section
	$(".home section").hover(function(){
		$(".home section").removeClass("expand");
		$(".home section").addClass("shrink");
		$(this).removeClass("shrink");
		$(this).addClass("expand");
	}, function(){
		$(".home section").removeClass("expand");
		$(".home section").removeClass("shrink");
	})

	//Parallax effect on the three sections if people scroll down  
    window.onscroll = function() {
        var speed = 10.0;
        var speed1 = 6.0;
        var speed2 = 7.0;

        $("section.people").css("background-position-y", (-window.pageYOffset / speed) + "px");
        $("section.jobs").css("background-position-y", (-window.pageYOffset / speed1) + "px");
        $("section.places").css("background-position-y", (-window.pageYOffset / speed2) + "px");
    }

    //Move Explanation Part from top so it is below the .welcome_part
    $(".explanation_part").css("margin-top", windowHeight + "px")
            

});