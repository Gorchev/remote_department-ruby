Rails.application.routes.draw do
  get 'companies/index'
  get 'companies/show'
  get 'companies/crate'
  get 'companies/new'
  get 'companies/edit'
  get 'companies/update'

  get 'people/index'

  devise_for :admins, controllers: { registrations: "admins/registrations" }
  devise_for :companies
  devise_for :people
  resources :jobs

  get 'tags/:tag', to: 'people#index', as: :tag 

  match ':controller(/:action(/:id))', :via => :get

  root 'jobs#index'
end
