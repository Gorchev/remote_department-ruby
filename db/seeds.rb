# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

	category = Category.create([{ name: 'Front End Developer' }])

	person = Person.create(email: 'mladen.gorchev@gmail.com', password: 'password', password_confirmation: 'password')

	company = Company.create(email: 'denitest1@gmail.com', password: 'password3', password_confirmation: 'password3')