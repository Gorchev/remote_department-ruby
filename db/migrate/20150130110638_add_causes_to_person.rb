class AddCausesToPerson < ActiveRecord::Migration
  def change
    add_column :people, :causes, :text
  end
end
