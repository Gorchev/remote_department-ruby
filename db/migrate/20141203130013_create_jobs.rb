class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :company
      t.string :website
      t.string :hq
      t.text :description
      t.string :banner
      t.string :url
      t.references :category

      t.timestamps
    end
  end
end
