class RenameColumnCompanyInJobs < ActiveRecord::Migration
  def change
  	rename_column :jobs, :company, :from_company
  end
end
